[Khmer restaurant in Siem Reap](https://www.changkrankhmer.com/)

Changkran Khmer is one of the top 10 Cambodian restaurant in Siem Reap Cambodia.

Enjoy the best of Khmer Fine Dining Cuisine in a beautiful and spacious traditional wooden house tucked away from Pub street.

We do serve also [vegetarian and vegan dishes](https://changkrankhmer.com/blog/vegetarian-restaurant-siem-reap/)

[Cooking classes in Siem Reap](https://changkrankhmer.com/siem-reap-cooking-class/) are available with Chef Mongkol and his team.



Changkran Khmer Restaurant Mondol 3 Village, Sangkat Sla Kram Siem Reap District Siem Reap Province 17251 Telephone 078 481 117 061 966 201 Email changkrankhmer@gmail.com

[Google Maps](http://bit.ly/2ONNrat)